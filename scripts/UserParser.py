import random

# Set seed value so sample can be repeated
random.seed(1)
# Select 360 random indices from population
indices = random.sample(range(0, 360001), 360)

# Count for current index
cnt = 0

# Open input and output file
with open('usersha1-artmbid-artname-plays.tsv', 'r') as tsvin, open('user-sample.tsv', 'w') as tsvout:
	curUser = ""
	# Loop through each line in file
	for line in tsvin:
		lineItems = line.split('\t')
		[uid, artistid, artistname, plays] = lineItems
		# If user is empty, set initial index
		if curUser == "":
			curUser = uid
			cnt += 1
		# If user changed, update index
		elif curUser != uid:
			curUser = uid
			cnt += 1
		# If current user in index, append to output
		if cnt in indices:
			newLine = uid + '\t' + artistname + '\t' + plays
			tsvout.write(newLine)
