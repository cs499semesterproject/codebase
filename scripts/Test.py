import random

items = dict()

with open('user-sample.tsv', 'r') as tsvin:
	curUser = ""
	for line in tsvin:
		lineItems = line.split('\t')
		[uid, artistid, artistname, plays] = lineItems

		if curUser == "":
			curUser = uid
			items[curUser] = []
		elif curUser != uid:
			curUser = uid
			items[curUser] = []
		
		items[curUser] = items[curUser] + [[artistid, artistname, plays]]
	print(len(items))
